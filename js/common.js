/**
 * Created by 10243 on 2019/9/5.
 */
/*
$(window).scroll(function(){
    if($(window).scrollTop() >= 10){
        $(".ding").css({"background": "rgba(85, 94, 118, 0.62)"});
    } else{
        $(".ding").css({"background": "rgba(85, 94, 118, 0)"});
    }
});*/

/*导航栏移动下划线*/
(function () {
    $(".js-nav > li").hover(function () {
        var t = $(".js-nav").parent().offset().left,
            i = 15,
            n = $(this),
            e = n.offset().left + i;
        $(".js-menu-act").css({
            left: e,
            width: n.width() - 2 * i,
            opacity: 1
        })
    }, function () {
        $(".js-menu-act").css({
            opacity: 0
        })
    })
})()

/*判断当前是移动端*/
$(document).ready(function (){
    var phoneWidth=$(window).width();
    if(phoneWidth<768) {
        $(window).scroll(function () {
            /*如果滚动 则隐藏背景*/
            if ($(window).scrollTop() >= 10) {
                $(".ding").css({"background": "rgba(85, 94, 118, 0)"});
                $(".logo").hide();
                $(".navbar-collapse ").hide();
            } else {
                $(".ding").css({"background": "rgba(85, 94, 118, 0)"});
                $(".logo").show();
                $(".navbar-collapse ").hide();
            }
        });

        /*点击空白或按钮隐藏*/
        document.onclick = function(e) {
            $(".navbar-collapse").hide();
        }
        $('.navbar-toggle').on("click", function(e) {
            if($(".navbar-collapse").css("display") == "none") {
                $(".navbar-collapse").show();
                $(".logo").show();
                $(".ding").css({"background": "rgba(0, 0, 0, 0.61)"});
                $(".collapse ").css({"background": "rgba(0, 0, 0, 0.61)"});
            } else {
                $(".navbar-collapse").hide();
                $(".ding").css({"background": "rgba(85, 94, 118, 0)"});
            }
            e = e || event;
            stopFunc(e);
        });

        $('.navbar-collapse').on("click", function(e) {
            e = e || event;
            stopFunc(e);
        });
        function stopFunc(e) {
            e.stopPropagation ? e.stopPropagation() : e.cancelBubble = true;
        }

    }
});

